#the detection is super shitty and makes me want to die, but it's all i know how to do right now so cry more
import cv2
import json

def detectFace(img, gray, xConst, yConst, port): 

    faceCas = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')
    
    iCame = json.load(open('calibration.json', 'r'))

    face = faceCas.detectMultiScale(gray, 1.2, 3)

    for (x, y, w, h) in face:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)

        x = (iCame['xPosLeft'] + (x / xConst)) - iCame["xOffSet"]

        y = (iCame['yPosTop'] + (y / yConst)) + iCame["yOffSet"]

        coords = "{},{}".format((x),(y))
        print(coords)
        port.write(coords.encode())

    return img