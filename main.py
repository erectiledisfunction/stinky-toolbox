from json.decoder import JSONDecodeError
import calibrate
import json
import detection as dec
import sizematters as size
import time
import calculate
import cv2 as cv
import serial

#calls to reset servo's positions to default whenever the application ends
def resetPos():
    port.write("90,90".encode())

#To read video capture
assBeLike = cv.VideoCapture(1)

if not assBeLike.isOpened():
    print("not openable you dipshit")
    exit()

window = cv.namedWindow = "stinky shidd" 

width = 1280
height = 720

resize = size.rez(assBeLike, width, height)

#time wait
time.sleep(1)

#open the port and run main function
try:
    port = serial.Serial("COM3", 9600)
except:
    raise Exception("Cannot open port")
    
    
try:
    jsonFile = open("calibration.json", "r")
except Exception:
    port.close()

    left = calibrate.calibrate()
    right = calibrate.calibrate()
    
    raise print("JSON not detected, running calibration module")
    
settings = json.load(jsonFile)

xConst, yConst = calculate.calc(settings['xPosRight'], settings['xPosLeft'], settings['yPosTop'], settings['yPosBot'], width, height)

while 1:
    #Captures every frame of the video, processes it and then display it
    ret, cum = assBeLike.read()
    cum = cv.flip(cum, 1)
    grayScale = cv.cvtColor(cum, cv.COLOR_BGR2GRAY)
    time.sleep(0.015)
    dec.detectFace(cum, grayScale, xConst, yConst, port)
    cv.imshow(window, cum)
        
    if cv.waitKey(10) == 27:
        resetPos()
        break

time.sleep(1)

cv.destroyAllWindows()
port.close()
jsonFile.close()