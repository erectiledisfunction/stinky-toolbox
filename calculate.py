def calc(xRight, xLeft, yUp, yDown, width, height):
    servoXConst = width / (xRight - xLeft)
    servoYConst = height / (yDown - yUp)

    return servoXConst, servoYConst